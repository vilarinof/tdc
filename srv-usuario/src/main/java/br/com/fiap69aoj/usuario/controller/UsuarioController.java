package br.com.fiap69aoj.usuario.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.fiap69aoj.usuario.model.Usuario;
import br.com.fiap69aoj.usuario.service.UsuarioService;

@RestController
public class UsuarioController {
		
	@Autowired
	private UsuarioService service;	
	
	@GetMapping("/usuarios/{idAcesso}")
	public ResponseEntity<Usuario> listarUsuarioPorId(@PathVariable String idAcesso) {
		return ResponseEntity.ok(service.obterDadosUsuarioPorId(idAcesso));
	}
	
	@PostMapping("/usuarios/")
	public ResponseEntity<HttpStatus> incluirUsuario(@RequestBody Usuario usuario) {
		service.incluirUsuario(usuario);
		return ResponseEntity.status(HttpStatus.CREATED).build();
	}
}
