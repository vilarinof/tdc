package br.com.fiap69aoj.chamados.model;

import java.time.LocalDate;

public class Chamado {
	
	private String idAcessoUsuario;	
	private String comentario;	
	private LocalDate dataCriacao;	
	private String status;
	
	public static class BuildChamado {
		
		private String idAcessoUsuario;	
		private String comentario;	
		private LocalDate dataCriacao;	
		private String status;
		
		public BuildChamado doUsuario(String idAcessoUsuario) {
			this.idAcessoUsuario = idAcessoUsuario;	
			return this;
		}
		
		public BuildChamado comComentario(String comentario) {
			this.comentario = comentario;	
			return this;
		}	
		
		public BuildChamado deStatus(String status) {
			this.status = status;	
			return this;
		}	
		
		public BuildChamado criadoEm(LocalDate dataCriacao) {
			this.dataCriacao = dataCriacao;	
			return this;
		}	
		
		public Chamado build() {
			return new Chamado(this);
		}
	}

	private Chamado(BuildChamado builder) {
		idAcessoUsuario = builder.idAcessoUsuario;
		comentario = builder.comentario;
		dataCriacao = builder.dataCriacao;
		status = builder.status;
	}
	
	public Chamado() {}

	public String getIdAcessoUsuario() {
		return idAcessoUsuario;
	}

	public void setIdAcessoUsuario(String idAcessoUsuario) {
		this.idAcessoUsuario = idAcessoUsuario;
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public LocalDate getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(LocalDate dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
		
}
