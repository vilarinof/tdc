package br.com.fiap69aoj.chamados.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.fiap69aoj.chamados.eventos.ChamadoProducer;
import br.com.fiap69aoj.chamados.model.Chamado;
import br.com.fiap69aoj.chamados.service.ChamadoService;

@RestController
public class ChamadosController {
	
	@Autowired
	private ChamadoService chamadoService; 
	
	private ChamadoProducer chamadoProducer;
	
	public ChamadosController(ChamadoProducer chamadoProducer) {
        this.chamadoProducer = chamadoProducer;
    }
	
	@PostMapping("/chamados/")
	public ResponseEntity<HttpStatus> incluirChamado(@RequestBody Chamado chamado) {
		chamadoService.incluirUsuario(chamado);
		chamadoProducer.send(chamado);
		return ResponseEntity.status(HttpStatus.CREATED).build();
	}
	
	@GetMapping("/chamados/")
	public ResponseEntity<List<Chamado>> listarChamados() {
		return ResponseEntity.ok(chamadoService.listarChamados());
	}
	
	@GetMapping("/chamados/{id}")
	public ResponseEntity<Chamado> listarChamadoPorId(@PathVariable long id) {
		return ResponseEntity.ok(chamadoService.listarChamadosPorId(id));
	}

}
