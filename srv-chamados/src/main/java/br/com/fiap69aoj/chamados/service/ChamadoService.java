package br.com.fiap69aoj.chamados.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.fiap69aoj.chamados.dao.entity.ChamadoEntity;
import br.com.fiap69aoj.chamados.dao.repository.ChamadoRepository;
import br.com.fiap69aoj.chamados.exception.ChamadoNaoEncontradoException;
import br.com.fiap69aoj.chamados.model.Chamado;
import br.com.fiap69aoj.chamados.model.StatusEnum;

@Service
public class ChamadoService {
	
	@Autowired
	private ChamadoRepository chamadoRepository;

	public void incluirUsuario(Chamado chamado) {
		ChamadoEntity entity = new ChamadoEntity();
		entity.setIdAcessoUsuario(chamado.getIdAcessoUsuario());
		entity.setComentario(chamado.getComentario());
		entity.setDataCriacao(LocalDate.now());
		entity.setStatus(StatusEnum.ABERTO.getDescricao());
		chamadoRepository.save(entity);		
	}

	public List<Chamado> listarChamados() {
		List<ChamadoEntity> consulta = chamadoRepository.findAll();
		List<Chamado> retorno = new ArrayList<Chamado>();
		
		for (ChamadoEntity chamadoRetorno : consulta) {
			retorno.add(buildChamadoFromEntity(chamadoRetorno));
		}		
		return retorno;
	}
	
	public Chamado listarChamadosPorId(long id) {
		Optional<ChamadoEntity> chamado = chamadoRepository.findById(id);
		if(chamado.isPresent()) {
			return this.buildChamadoFromEntity(chamado.get());
		}
		throw new ChamadoNaoEncontradoException(id);
		
	}
	
	private Chamado buildChamadoFromEntity(ChamadoEntity entity) {
		return new Chamado.BuildChamado()
				.doUsuario(entity.getIdAcessoUsuario())
				.criadoEm(entity.getDataCriacao())
				.comComentario(entity.getComentario())
				.deStatus(entity.getStatus())
				.build();
	}



}
